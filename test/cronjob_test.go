package test

import (
	"testing"

	"github.com/gruntwork-io/terratest/modules/helm"
	"github.com/stretchr/testify/assert"
	batchv1 "k8s.io/api/batch/v1beta1"
)

func TestCronJob(t *testing.T) {
	// Path to the helm chart we will test
	helmChartPath := ".."

	// Setup the args. For this test, we will set the following input values:
	options := &helm.Options{
		SetFiles: map[string]string{"values": "values-test.yaml"},
	}

	// Run RenderTemplate to render the template and capture the output.
	output := helm.RenderTemplate(t, options, helmChartPath, "cronjob", []string{"templates/cronjob.yaml"})

	// Now we use kubernetes/client-go library to render the template output into the Pod struct. This will
	// ensure the Pod resource is rendered correctly.
	var testObj batchv1.CronJob
	helm.UnmarshalK8SYaml(t, output, &testObj)

	// Finally, we verify the pod spec is set to the expected container image value
	assert.Equal(t, "postgres-backup", testObj.ObjectMeta.Name)
	assert.Contains(t, testObj.ObjectMeta.Labels, "app.kubernetes.io/name")
	assert.Contains(t, testObj.ObjectMeta.Labels, "app.kubernetes.io/instance")
	assert.Contains(t, testObj.ObjectMeta.Labels, "app.kubernetes.io/version")
	assert.Contains(t, testObj.ObjectMeta.Labels, "app.kubernetes.io/managed-by")
	assert.Contains(t, testObj.ObjectMeta.Labels, "helm.sh/chart")
}
