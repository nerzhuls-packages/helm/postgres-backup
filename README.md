[![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/postgres-backup)](https://artifacthub.io/packages/search?repo=postgres-backup)

# postgres-backup

This helm chart is intended to backup a PostgreSQL instance, dumping data on a NFS and publish it to S3

## Getting started

## Roadmap

Some parts can be made conditional and storage modified depending on the community needs